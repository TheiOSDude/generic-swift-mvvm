//
//  LDBAnalytics.swift
//  GenericSwift
//
//  Created by Lee Burrows on 14/11/2018.
//  Copyright © 2018 The iOS Dude. All rights reserved.
//

import Foundation

class LDBAnalytics: AnalyticsProtocol {
    func track(value: String) {
        print("📈tracking: \(value)")
    }
}
