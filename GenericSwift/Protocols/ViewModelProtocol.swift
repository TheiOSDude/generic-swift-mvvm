//
//  ViewModel.swift
//  GenericSwift
//
//  Created by Lee Burrows on 25/10/2018.
//  Copyright © 2018 The iOS Dude. All rights reserved.
//

import Foundation
import UIKit

protocol AnalyticsProtocol {
    func track(value: String)
}

protocol ViewModelProtocol {
    var analyticsProvider: AnalyticsProtocol? { get }
    func viewReady()
    var title: String { get }
    init()
}

protocol ViewModelBased: class {
    associatedtype ViewModel: ViewModelProtocol
    var viewModel: ViewModel { get set }
    func bindToViewModel()
    
}

protocol NibBased: class {
    static func instance() -> Self
}

extension NibBased where Self: UIViewController {
    static func instance() -> Self {
       return Self.init(nibName: String.init(describing: self), bundle: nil)
    }
}

extension ViewModelBased where Self: NibBased & UIViewController {
    static func instance(with viewModel: ViewModel) -> Self {
        let vc = Self.instance()
        vc.viewModel = viewModel
        return vc
    }
}

class BaseViewController<T: ViewModelProtocol>: UIViewController, ViewModelBased  {
    
    var viewModel = T.init()
    typealias ViewModel = T
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindToViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewReady()
    }
    
    func bindToViewModel() {
        title = viewModel.title
    }
}

