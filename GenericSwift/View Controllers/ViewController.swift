//
//  ViewController.swift
//  GenericSwift
//
//  Created by Lee Burrows on 25/10/2018.
//  Copyright © 2018 The iOS Dude. All rights reserved.
//

import UIKit

class ViewController: BaseViewController<HomeViewModel> {
    
    override func bindToViewModel() {
        super.bindToViewModel()
        viewModel.proceedNext = { [unowned self] in
            self.showNextViewController()
        }
    }

    private func showNextViewController() {
        navigationController?.pushViewController(NextViewController.instance(), animated: true)
    }
    
    @IBAction func tappedNext(_ sender: Any) {
        viewModel.didTapNext()
    }
}

