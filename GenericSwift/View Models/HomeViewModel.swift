//
//  HomeViewModel.swift
//  GenericSwift
//
//  Created by Lee Burrows on 25/10/2018.
//  Copyright © 2018 The iOS Dude. All rights reserved.
//

import Foundation

struct HomeViewModel: ViewModelProtocol {
    var analyticsProvider: AnalyticsProtocol? = LDBAnalytics()
    var proceedNext: (()->())?
    var title: String {
        return "Home"
    }
    
    func viewReady() {
        analyticsProvider?.track(value: "Did show home")
    }
    
    func didTapNext() {
        analyticsProvider?.track(value: "didtapnext")
        proceedNext?()
    }
    
    
}
