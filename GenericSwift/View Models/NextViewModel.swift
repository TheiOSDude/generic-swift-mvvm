//
//  NextViewModel.swift
//  GenericSwift
//
//  Created by Lee Burrows on 13/11/2018.
//  Copyright © 2018 The iOS Dude. All rights reserved.
//

import Foundation

struct NextViewModel: ViewModelProtocol {
    var analyticsProvider: AnalyticsProtocol? = LDBAnalytics()
    
    var title: String {
        return "Next"
    }
    
    func viewReady() {
        analyticsProvider?.track(value: "next")
    }
}
